# photon-setup

Initial setup script for [Photon OS](https://github.com/vmware/photon).

## Usage

```bash
curl https://gitlab.com/iwaseyusuke/photon-setup/-/raw/main/setup.sh | bash
```

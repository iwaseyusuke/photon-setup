#!/usr/bin/env bash

set -eux

# Update all packages
tdnf update -y

# Disable password expiration
passwd -x -1 root

# Prepare SSH key
if [ ! -f "$HOME/.ssh/id_rsa" ]; then
  ssh-keygen -t rsa -b 4096 -q -f "$HOME/.ssh/id_rsa" -N ""
  cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys
  chmod 600 ~/.ssh/authorized_keys
fi

# Insert some rules into iptables config
IP4SAVE=/etc/systemd/scripts/ip4save
function insert_rule_into_ip4save () {
  if grep -e "$*" $IP4SAVE > /dev/null; [ $? -eq 0 ]; then
    return 0
  fi
  cp $IP4SAVE $IP4SAVE.org
  head -n -2 $IP4SAVE.org > $IP4SAVE
  echo "$*" >> $IP4SAVE
  tail -n 2 $IP4SAVE.org >> $IP4SAVE
  return 0
}

## Insert a rule to allow ICMP Echo Reply
insert_rule_into_ip4save "-A INPUT -p icmp -j ACCEPT"

# Reload iptables
systemctl daemon-reload
systemctl restart iptables

# Install packages
tdnf install -y \
  bindutils \
  git \
  gawk \
  tar \
  unzip \
  wget
